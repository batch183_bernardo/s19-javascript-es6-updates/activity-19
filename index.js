// console.log("Hello Word!")



let getCube = 2 ** 3
console.log(`The cube of 2 is ${getCube}`)

const address = [258, "Washinton Ave NW", "California", 90011]

const [number, street, city, zipcode] = address
console.log(`I live at ${number} ${street}, ${city} ${zipcode}`);

let animal = {
	name: "Lolong",
	type: "crocodile",
	weight: 1075,
	ft: 20,
	inches: 3
}

console.log(`${animal.name} was a saltwater ${animal.type}. He weighed at ${animal.weight} kgs with measurement of ${animal.ft} ft ${animal.inches} in.`);

let numbers = [1, 2, 3, 4, 5]

numbers.forEach(number => console.log(number))


let reduceNumber = numbers.reduce((acc, cur) => acc + cur) ;

console.log(reduceNumber);


class Dog{
	constructor(name, age, breed){
		this.name = name
		this.age = age
		this.breed = breed
	}
}

const newDog = new Dog ("Frankie", 5, "Miniature Dachshund");
console.log(newDog)

